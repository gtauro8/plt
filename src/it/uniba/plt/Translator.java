package it.uniba.plt;

public class Translator {

	public static final String NIL = "nil";

	private final TranslatorData data = new TranslatorData("", 0);

	public Translator(final String inputPhrase) {

		this.data.phrase = inputPhrase;

	}

	public String getPhrase() {

		return data.phrase;

	}

	private boolean endsWithPunctuation() {

		return (data.phrase.endsWith(".") || data.phrase.endsWith(",") || data.phrase.endsWith("!")
				|| data.phrase.endsWith("?") || (data.phrase.endsWith(";")));

	}

	private boolean startWithVowel() {
		return (data.phrase.startsWith("a") || data.phrase.startsWith("e") || data.phrase.startsWith("i")
				|| data.phrase.startsWith("o") || data.phrase.startsWith("u"));
	}

	private boolean endWithVowel() {
		return (data.phrase.endsWith("a") || data.phrase.endsWith("e") || data.phrase.endsWith("i")
				|| data.phrase.endsWith("o") || data.phrase.endsWith("u"));
	}

	private boolean consonantVerify(final char c) {

		return !(c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u');

	}

	private int initialsConsonantsCounter() {
		int i = 0;

		for (i = 0; i < data.phrase.length(); i++) {
			if (consonantVerify(data.phrase.charAt(i))) {
				data.consCount++;
			} else {

				return data.consCount++;
			}

		}

		return data.consCount++;
	}

	public String translatePhraseWithPunctuation(final String str) {

		int temp = 0;
		char punctTemp = 0;
		String tempStr = "";

		for (int i = 0; i < str.length(); i++) {

			if (str.charAt(i) == '.' || str.charAt(i) == ',' || str.charAt(i) == '!' || str.charAt(i) == '?'
					|| str.charAt(i) == ';') {
				temp = i;
				punctTemp = str.charAt(i);
			}

		}
		tempStr = str.substring(0, temp);
		final Translator t = new Translator(tempStr);
		tempStr = t.translate();

		return tempStr + punctTemp;

	}

	public String findSymbolsAndDivide(final String str) {

		String tmp = "";
		String tmp1 = "";

		if (str.contains(" ")) {
			final String[] s = str.split(" ");
			final Translator t = new Translator(s[0]);
			tmp = t.translate();

			for (int i = 1; i < s.length; i++) {

				final Translator t1 = new Translator(s[i]);
				tmp1 = t1.translate();
				tmp = String.join(" ", tmp, tmp1);
			}

		} else {
			final String[] s = str.split("-");

			final Translator t = new Translator(s[0]);
			tmp = t.translate();

			for (int i = 1; i < s.length; i++) {

				final Translator t1 = new Translator(s[i]);
				tmp1 = t1.translate();
				tmp = String.join("-", tmp, tmp1);

			}

		}

		return tmp;
	}

	public String translate() {

		if (!data.phrase.equals("")) {

			if (endsWithPunctuation()) {
				return translatePhraseWithPunctuation(data.phrase);
			}

			if (data.phrase.split(" ").length > 1) {
				return findSymbolsAndDivide(data.phrase);
			}
			if (data.phrase.split("-").length > 1) {
				return findSymbolsAndDivide(data.phrase);
			}

			if (startWithVowel())
				if (data.phrase.endsWith("y")) {

					return data.phrase + "nay";

				} else if (endWithVowel()) {

					return data.phrase + "yay";

				} else if (!endWithVowel()) {

					return data.phrase + "ay";
				}

			if (!startWithVowel()) {

				final int cons = initialsConsonantsCounter();

				if (cons == 1) {

					return data.phrase.substring(1) + data.phrase.charAt(0) + "ay";

				} else {

					return data.phrase.substring(cons) + data.phrase.substring(0, cons) + "ay";
				}

			}
		}

		return NIL;

	}

}
