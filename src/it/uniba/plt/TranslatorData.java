package it.uniba.plt;

public class TranslatorData {
	public String phrase;
	public int consCount;

	public TranslatorData(String phrase, int consCount) {
		this.phrase = phrase;
		this.consCount = consCount;
	}
}