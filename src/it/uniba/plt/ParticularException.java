package it.uniba.plt;

public class ParticularException extends Exception {

	public ParticularException(String message) {

		super(message);

	}

}
