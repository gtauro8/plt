package it.uniba.plt;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "Hello World";
		Translator t = new Translator(inputPhrase);

		assertEquals("Hello World", t.getPhrase());
	}

	@Test
	public void testEmpyPhrase() {
		String inputPhrase = "";
		Translator t = new Translator(inputPhrase);

		assertEquals(Translator.NIL, t.translate());
	}

	@Test
	public void testPhraseStartWithAAndEndWithY() {
		String inputPhrase = "any";
		Translator t = new Translator(inputPhrase);

		assertEquals("anynay", t.translate());
	}

	@Test
	public void testPhraseStartWithUAndEndWithY() {
		String inputPhrase = "utility";
		Translator t = new Translator(inputPhrase);

		assertEquals("utilitynay", t.translate());
	}

	@Test
	public void testPhraseStartWithVowelAndEndWithVowel() {
		String inputPhrase = "apple";
		Translator t = new Translator(inputPhrase);

		assertEquals("appleyay", t.translate());
	}

	@Test
	public void testPhraseStartWithVowelAndEndWithConsonant() {
		String inputPhrase = "ask";
		Translator t = new Translator(inputPhrase);

		assertEquals("askay", t.translate());
	}

	@Test
	public void testPhraseStartWithAConsonant() {
		String inputPhrase = "rock";
		Translator t = new Translator(inputPhrase);

		assertEquals("ockray", t.translate());
	}

	@Test
	public void testPhraseStartWithMoreWordsDividedBySpaces() {
		String inputPhrase = "planet eart hello";
		Translator t = new Translator(inputPhrase);

		assertEquals("anetplay eartay ellohay", t.translate());
	}

	@Test
	public void testPhraseStartWithMoreWordsDividedByDashes() {
		String inputPhrase = "planet-eart-hello";
		Translator t = new Translator(inputPhrase);

		assertEquals("anetplay-eartay-ellohay", t.translate());
	}

	@Test
	public void testPhraseStartWithMoreWordsDividedByDashesAndSpaces() {
		String inputPhrase = "planet eart-hello";
		Translator t = new Translator(inputPhrase);

		assertEquals("anetplay eartay-ellohay", t.translate());
	}

	@Test
	public void testPhraseEndsWithPoint() {
		String inputPhrase = "planet eart-hello.";
		Translator t = new Translator(inputPhrase);

		assertEquals("anetplay eartay-ellohay.", t.translate());
	}

	@Test
	public void testPhraseEndsWithComma() {
		String inputPhrase = "planet eart-hello,";
		Translator t = new Translator(inputPhrase);

		assertEquals("anetplay eartay-ellohay,", t.translate());
	}

	@Test
	public void testPhraseEndsWithQuestionMark() {
		String inputPhrase = "planet eart-hello?";
		Translator t = new Translator(inputPhrase);

		assertEquals("anetplay eartay-ellohay?", t.translate());
	}

	@Test
	public void testPhraseEndsWithEsclamativePoint() {
		String inputPhrase = "planet eart-hello!";
		Translator t = new Translator(inputPhrase);

		assertEquals("anetplay eartay-ellohay!", t.translate());
	}

	@Test
	public void testPhraseEndsWithSemicolon() {
		String inputPhrase = "planet eart-hello;";
		Translator t = new Translator(inputPhrase);

		assertEquals("anetplay eartay-ellohay;", t.translate());
	}

	@Test
	public void testPhraseEndsWithPunctuationBetweenWords() {
		String inputPhrase = "planet. eart-hello;";
		Translator t = new Translator(inputPhrase);

		assertEquals("anetplay. eartay-ellohay;", t.translate());
	}

	@Test
	public void testPhraseThatRaiseException() throws ParticularException {
		String inputPhrase = "planet.  eart-hello;";
		Translator t = new Translator(inputPhrase);

		t.translate();
	}

}
